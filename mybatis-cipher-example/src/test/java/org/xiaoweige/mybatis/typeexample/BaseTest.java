package org.xiaoweige.mybatis.typeexample;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * BaseTest
 * @author Jerry.hu
 * @summary BaseTest
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description BaseTest
 * @since 2018-05-03 16:22
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
//        ,properties = {"logging.path=logs",
//                "spring.profiles.active=test"
//        }
        )
@DirtiesContext
//@Transactional
//@Rollback
public class BaseTest {

}
