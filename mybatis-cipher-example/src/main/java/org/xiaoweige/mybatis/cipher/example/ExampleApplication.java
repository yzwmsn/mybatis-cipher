package org.xiaoweige.mybatis.cipher.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xiaoweige.mybatis.annotation.EnableEncrypt;

@SpringBootApplication
@EnableEncrypt
public class ExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
    }
}
