package org.xiaoweige.mybatis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * encrypted
 * @author Jerry.hu
 * @summary encrypted
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description encrypted
 * @since 2018-06-12 01:11
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface EnableCipher {
    /**
     * 加解密模式
     * @return CipherType
     */
    CipherType value() ;

    /**
     * 批量加密 默认不开启批量操作
     * ps 如果需要使用批量操作请实现
     *
     * @return true 开启加密
     */
    boolean EnableBatch() default false;


    enum CipherType{
        /**
         * 加密
         */
        ENCRYPT,
        /**
         * 解密
         */
        DECRYPT,
    }
}
